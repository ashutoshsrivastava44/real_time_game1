var express = require('express');

var app = express()
var server = app.listen(7890);
var io = require('socket.io').listen(server);

app.set('view options', { layout: false });

app.use(express.static('public'));

app.get('/', function(req, res) {
  res.render('index.ejs');
});

// handle incoming connections from clients
io.sockets.on('connection', function(socket) {
    // once a client has connected, we expect to get a ping from them saying what room they want to join
	var users=[];
	
    socket.on('switchRoom', function(newroom) {
		socket.leave(socket.room);
		socket.join(newroom);
		socket.emit('updateroom','SERVER.you are connected to'+newroom);
		socket.broadcast.to(socket.room).emit('updateroom','SERVER',socket.username+'has left this room');
		socket.room=newroom;
		socket.broadcast.to(newroom).emit('updateroom','SERVER',socket.username+'has joined this room');
		
    });
});