// set-up a connection between the client and the server
var socket = io.connect();

// let's assume that the client page, once rendered, knows what room it wants to join
var room = "abc123";
socket.emit('switchRoom', newRoom);
socket.on('updaterooms', function(rooms,current_room) {
	
   // logic need to be implemented
   
   socket.emit('room', room);
});

socket.on('message', function(data) {
   console.log('Incoming message:', data);
});